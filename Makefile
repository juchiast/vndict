default:
	cargo build --release
install:
	install ./vndict /usr/bin
	install -d /opt/vndict
	install -d /opt/vndict/res
	install ./target/release/vndict /opt/vndict/
	install -m 644 ./res/index.txt /opt/vndict/res/
	install -m 644 ./res/dict.txt /opt/vndict/res/
