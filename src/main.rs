extern crate ncurses;
use ncurses::*;
use ncurses::CURSOR_VISIBILITY::*;
use std::cmp::*;

struct Dict {
    indexes: Vec<String>,
    contents: Vec<String>,
}

use std::io;
use std::io::Read;
use std::path::Path;
use std::fs::File;
impl Dict {
    fn new<P: AsRef<Path>>(index_path: P, content_path: P) -> io::Result<Dict> {
        let mut s = String::new();
        File::open(index_path)?.read_to_string(&mut s)?;
        let indexes = s.lines().map(|x| x.to_owned()).collect::<Vec<_>>();

        let mut s = String::new();
        File::open(content_path)?.read_to_string(&mut s)?;
        // A space before \n is required for wrap_text() to work
        let contents = s.lines().map(|x| x.replace("\\n", " \n")).collect::<Vec<_>>();

        if contents.len() != indexes.len() {
            Err(io::ErrorKind::InvalidData.into())
        } else {
            Ok(Dict {
                contents: contents,
                indexes: indexes,
            })
        }
    }

    fn find(&self, s: &String) -> Option<&String> {
        self.indexes.binary_search(s).ok().and_then(|x| self.contents.get(x))
    }
}

struct Viewer<'v> {
    current_line: usize,
    text: &'v str,
    rendered: Vec<&'v str>,
    size: (usize, usize),
}

fn wrap_text(text: &str, size: (usize, usize)) -> Vec<&str> {
    text.lines().flat_map(|mut text| {
        let split = {
            let iter = || text.char_indices().enumerate()
                .filter(|&(_, (_, c))| c.is_whitespace()).map(|(i, (x, _))| (i, x));
            let mut tmp = iter().zip(iter().skip(1))
                .map(|((_, x), (i, _))| (i, x))
                .fold(vec![(0, 0)], |mut vec, (i, x)| {
                    if vec.last().unwrap().0 / size.0 != i / size.0 {
                        vec.push((i, x));
                    }
                    vec
                });
            tmp.reverse();
            tmp
        };
        let mut this = split.into_iter().map(|(_, x)| {
            let tmp = text.split_at(x);
            text = tmp.0;
            tmp.1
        }).collect::<Vec<_>>();
        this.reverse();
        this
    }).collect()
}

impl<'v> Viewer<'v> {
    fn new(text: &str, size: (usize, usize)) -> Viewer {
        Viewer {
            current_line: 0,
            text: text,
            rendered: wrap_text(text, size),
            size: size,
        }
    }

    fn draw(&mut self, size: (usize, usize)) {
        curs_set(CURSOR_INVISIBLE);
        if self.size != size {
            self.rendered = wrap_text(self.text, size);
            self.current_line = 0;
        }
        for (i, s) in self.rendered.iter().skip(self.current_line).take(self.size.1).enumerate() {
            mvprintw(i as i32, 0, s);
        }
    }
}

struct Searcher {
    current_input: String,
    input_pos: usize,
    index: usize,
    current_line: usize,
}
impl Searcher {
    fn new() -> Searcher {
        Searcher {
            current_input: String::new(),
            input_pos: 0,
            index: 0,
            current_line: 0,
        }
    }

    fn draw(&self, dict: &Dict, size: (usize, usize)) {
        curs_set(CURSOR_VISIBLE);
        printw(&self.current_input);
        mv(1, 0);
        printw(&"-".repeat(size.0));
        for (i, s) in dict.indexes.iter().skip(self.index).take(size.1 - 2).enumerate() {
            if i == self.current_line {
                attron(A_REVERSE());
                mvprintw(i as i32 + 2, 0, s);
                printw(&" ".repeat(size.0-s.len()));
                attroff(A_REVERSE());
            } else { 
                mvprintw(i as i32 + 2, 0, s);
            }
        }
    }

    fn current_dict_index(&self) -> usize {
        self.index + self.current_line
    }
}

fn inc(x: &mut usize, lim: usize) -> bool {
    if *x < lim { *x += 1; true }
    else { false }
}
fn dec(x: &mut usize, lim: usize) -> bool {
    if *x > lim { *x -= 1; true }
    else { false }
}

fn main() {
    use std::ascii::AsciiExt;

    let dict = Dict::new("res/index.txt", "res/dict.txt").expect("Load data files error.");
    setlocale(LcCategory::all, "C");
    let window = initscr();
    noecho();
    keypad(stdscr(), true);

    let get_size = || (getmaxx(window) as usize, getmaxy(window) as usize);
    let mut searcher = Searcher::new();
    let mut viewer = Viewer::new("", get_size());
    let mut searching = true;

    loop {
        let size = get_size();
        clear();
        if searching {
            searcher.draw(&dict, size);
        } else {
            viewer.draw(size);
        };
        wmove(window, 0, searcher.input_pos as i32);
        refresh();
        let key = getch();
        if searching {
            let prev = searcher.current_input.clone();
            match key {
                10 => {
                    searching = false;
                    let word = &dict.indexes[searcher.current_dict_index()];
                    let contents = dict.find(word).unwrap();
                    viewer = Viewer::new(contents, size);
                },
                KEY_UP => if !dec(&mut searcher.current_line, 0) {
                    dec(&mut searcher.index, 0);
                },
                KEY_DOWN => {
                    if !inc(&mut searcher.current_line, size.1 - 3) {
                        inc(&mut searcher.index, dict.indexes.len());
                    }
                    searcher.current_line = min(searcher.current_line, dict.indexes.len() - searcher.index);
                },
                KEY_LEFT => { dec(&mut searcher.input_pos, 0); },
                KEY_RIGHT => { inc(&mut searcher.input_pos, searcher.current_input.len()); },
                127 | 263 => if dec(&mut searcher.input_pos, 0) {
                    searcher.current_input.remove(searcher.input_pos);
                },
                k => if let Some(chr) = std::char::from_u32(k as u32) {
                    if chr.is_ascii() && !chr.is_control() {
                        searcher.current_input.insert(searcher.input_pos, chr);
                        searcher.input_pos += 1;
                    }
                },
            }
            if searcher.current_input != prev {
                searcher.current_line = 0;
                searcher.index = match dict.indexes.binary_search(&searcher.current_input) {
                    Ok(x) | Err(x) => x
                };
            }
        } else {
            match key {
                10 => searching = true,
                KEY_UP => { dec(&mut viewer.current_line, 0); },
                KEY_DOWN => { inc(&mut viewer.current_line, viewer.rendered.len()); },
                _ => (),
            }
        }
    }
}
